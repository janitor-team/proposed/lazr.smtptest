lazr.smtptest (2.0.3-3) UNRELEASED; urgency=medium

  [ Ondřej Nový ]
  * Bump Standards-Version to 4.4.1.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository, Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.
  * Apply multi-arch hints.
    + python-lazr.smtptest-doc: Add Multi-Arch: foreign.
  * Remove MIA uploader Barry Warsaw <barry@debian.org>. Closes: #970167

 -- Ondřej Nový <onovy@debian.org>  Fri, 18 Oct 2019 16:01:36 +0200

lazr.smtptest (2.0.3-2) unstable; urgency=medium

  * Team upload.
  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Remove ancient X-Python-Version field
  * d/control: Remove ancient X-Python3-Version field
  * Convert git repository from git-dpm to gbp layout
  * Use debhelper-compat instead of debian/compat.
  * Use Python 3 for building docs.
  * Drop Python 2 support (Closes: #936823).
  * Bump debhelper compat level to 12.
  * Bump standards version to 4.4.0 (no changes).
  * Use dh_auto_build instead of dh_installdocs for building docs.

  [ Barry Warsaw ]
  * Put DPMT in Maintainers and myself in Uploaders.

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol
  * d/control: Remove XS-Testsuite field, not needed anymore

 -- Ondřej Nový <onovy@debian.org>  Thu, 12 Sep 2019 15:21:24 +0200

lazr.smtptest (2.0.3-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Bumped Standards-Version to 3.9.6 with no other changes needed.
  * d/rules: Added PYBUILD_TEST_NOSE=1 to invoke the correct test runner.
  * d/watch: Use pypi.debian.net redirector.
  * d/copyright: Reorganize sections to keep lintian happy.

 -- Barry Warsaw <barry@debian.org>  Mon, 08 Jun 2015 18:34:21 -0400

lazr.smtptest (2.0.2-1) unstable; urgency=medium

  * New upstream release.
  * d/patches: Removed by git-dpm; fixed upstream.

 -- Barry Warsaw <barry@debian.org>  Wed, 20 Aug 2014 14:25:30 -0400

lazr.smtptest (2.0.1-1) unstable; urgency=medium

  * Initial release. (Closes: #758670)

 -- Barry Warsaw <barry@debian.org>  Tue, 19 Aug 2014 20:39:47 -0400
